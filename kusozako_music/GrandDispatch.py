
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .content_area.ContentArea import DeltaContentArea
from .queue.Queue import DeltaQueue


class DeltaGrandDispatch(DeltaEntity):

    def _delta_info_model(self):
        return self._queue.get_filter_model()

    def _delta_call_register_queue_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_queue_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_progress_object(self, object_):
        self._progress.register_listener(object_)

    def _delta_call_progress(self, user_data):
        self._progress.transmit(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        self._progress = FoxtrotTransmitter()
        self._queue = DeltaQueue(self)
        DeltaContentArea(self)
