
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

REPEAT_ALL = 0
REPEAT_ONE = 1
SHUFFLE = 2
NUMBER_OF_MODES = 3

ICON_NAMES = {
    REPEAT_ALL: "media-playlist-repeat-symbolic",
    REPEAT_ONE: "media-playlist-repeat-song-symbolic",
    SHUFFLE: "media-playlist-shuffle-symbolic"
    }

TOOLTIP_TEXT = {
    REPEAT_ALL: _("Repeat All"),
    REPEAT_ONE: _("Repeat One"),
    SHUFFLE: _("Shuffle")
    }


def get_next_to(current_mode):
    return (current_mode+1) % NUMBER_OF_MODES


def get_tooltip_text(current_mode):
    return TOOLTIP_TEXT[current_mode]
