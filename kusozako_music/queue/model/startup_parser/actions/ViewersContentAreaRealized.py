
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_music import QueueSignals
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver

DEVELOP_NAMES = [GLib.get_home_dir(), "kusozako-tech", "kusozako-music"]
DEVELOP_DIRECTORY = GLib.build_filenamev(DEVELOP_NAMES)
DIRECTORY = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC)


class DeltaViewersContentAreaRealized(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.VIEWERS_CONTENT_AREA_REALIZED

    def _control(self, user_data=None):
        if GLib.get_current_dir() == DEVELOP_DIRECTORY:
            directory = GLib.build_filenamev([DIRECTORY, ".test"])
        else:
            directory = DIRECTORY
        self._raise("delta > request parse directory", directory)
