
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity

NUMBER_OF_PARSING_FILES_AT_ONCE = 8


class DeltaFileEnumerator(DeltaEntity):

    def _on_break(self, file_enumerator):
        parent_gio_file = file_enumerator.get_container()
        directory = parent_gio_file.get_path()
        file_enumerator.close()
        self._raise("delta > break", directory)

    def _on_continue(self, files, file_enumerator):
        for file_info in files:
            gio_file = file_enumerator.get_child(file_info)
            if not file_info.get_is_hidden():
                self._raise("delta > continue", (gio_file, file_info))
        self._next_files_async(file_enumerator)

    def _next_files_async_finished(self, file_enumerator, task):
        files = file_enumerator.next_files_finish(task)
        if not files:
            self._on_break(file_enumerator)
        else:
            self._on_continue(files, file_enumerator)

    def _next_files_async(self, file_enumerator):
        file_enumerator.next_files_async(
            NUMBER_OF_PARSING_FILES_AT_ONCE,
            GLib.PRIORITY_DEFAULT,
            None,
            self._next_files_async_finished
            )

    def _on_enumerate_children_async_finished(self, source_object, task):
        file_enumerator = source_object.enumerate_children_finish(task)
        self._next_files_async(file_enumerator)

    def parse_directory_async(self, directory):
        gio_file = Gio.File.new_for_path(directory)
        gio_file.enumerate_children_async(
            "*",
            Gio.FileQueryInfoFlags.NONE,
            GLib.PRIORITY_DEFAULT,
            None,
            self._on_enumerate_children_async_finished
            )

    def __init__(self, parent):
        self._parent = parent
