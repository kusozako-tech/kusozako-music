
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_music import QueueSignals
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver


class DeltaInitialize(AlfaQueueSignalReceiver):

    def _on_initialize(self):
        model = self._enquiry("delta > model")
        path = GLib.random_int_range(0, len(model))
        data = QueueSignals.CURRENT_ROW_CHANGED, model[path]
        self._raise("delta > queue signal", data)
