
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver
from kusozako_music import QueueSignals

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Edit Metadata"),
            "message": "delta > edit metadata",
            "user-data": None,
            "close-on-clicked": True
        }
    ]
}

MODEL = [MAIN_PAGE]


class DeltaItemPopup(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.VIEWERS_POPUP_ITEM_POPUP

    def _delta_call_edit_metadata(self):
        param = QueueSignals.QUEUE_EDIT_METADATA, self._tree_row
        self._raise("delta > queue signal", param)

    def _control(self, param):
        tree_row, widget, x, y = param
        self._tree_row = tree_row
        self._popover_menu.popup_for_position(widget, x, y)

    def _on_initialize(self):
        self._popover_menu = DeltaPopoverMenu.new_for_model(self, MODEL)
