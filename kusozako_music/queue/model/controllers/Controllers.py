
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Initialize import DeltaInitialize
from .CurrentRow import DeltaCurrentRow
from .ShiftCurrentRow import DeltaShiftCurrentRow
from .QueueEditMetadata import DeltaQueueEditMetadata
from .SortColumnChanged import DeltaSortColumnChanged
from .SortOrderChanged import DeltaSortOrderChanged
from .viewers_popup.ViewersPopup import EchoViewersPopup


class EchoControllers:

    def __init__(self, parent):
        DeltaCurrentRow(parent)
        DeltaShiftCurrentRow(parent)
        DeltaQueueEditMetadata(parent)
        DeltaSortColumnChanged(parent)
        DeltaSortOrderChanged(parent)
        EchoViewersPopup(parent)
        DeltaInitialize(parent)         # must be initialized at last.
