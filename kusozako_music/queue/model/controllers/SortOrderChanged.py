
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver
from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes

A_TO_Z = Gtk.SortType.ASCENDING
Z_TO_A = Gtk.SortType.DESCENDING


class DeltaSortOrderChanged(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.QUEUE_SORT_ORDER_CHANGED

    def _control(self, sort_order):
        model = self._enquiry("delta > model")
        column_id, _ = model.get_sort_column_id()
        if column_id is None:
            column_id = ColumnTypes.TITLE
        model.set_sort_column_id(column_id, sort_order)
