
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import ColumnTypes
from .controllers.Controllers import EchoControllers
from .startup_parser.StartupParser import DeltaStartupParser
from .Metadata import DeltaMetadata


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_append_row(self, row):
        self.append(row)

    def _delta_call_file_found(self, gio_file):
        self._metadata.read_metadata(gio_file)

    def initialize_controllers(self):
        EchoControllers(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *ColumnTypes.TYPES)
        self._metadata = DeltaMetadata(self)
        DeltaStartupParser(self)
