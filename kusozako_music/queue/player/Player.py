
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from libkusozako3.Entity import DeltaEntity
from .controllers.Controllers import EchoControllers
from .watchers.Watchers import EchoWatchers
from .settings.Settings import EchoSettings


class DeltaPlayer(DeltaEntity):

    def _delta_info_playbin(self):
        return self._playbin

    def initialize_controllers(self):
        EchoControllers(self)
        EchoWatchers(self)
        EchoSettings(self)

    def __init__(self, parent):
        self._parent = parent
        Gst.init()
        self._playbin = Gst.ElementFactory.make("playbin")
        self._playbin.set_property("flags", Gst.StreamType.AUDIO)
        self._playbin.no_more_pads()
