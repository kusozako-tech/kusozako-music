
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CurrentRow import DeltaCurrentRow
from .Seek import DeltaSeek
from .Rewind import DeltaRewind
from .Forward import DeltaForward


class EchoControllers:

    def __init__(self, parent):
        DeltaCurrentRow(parent)
        DeltaSeek(parent)
        DeltaRewind(parent)
        DeltaForward(parent)
