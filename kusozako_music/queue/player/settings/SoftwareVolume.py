
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import DefaultSettings


class DeltaSoftwareVolume(DeltaEntity):

    def _refresh_software_volume(self):
        volume = self._enquiry(
            "delta > settings",
            DefaultSettings.SOFTWARE_VOLUME
            )
        playbin = self._enquiry("delta > playbin")
        playbin.set_property("volume", volume)

    def receive_transmission(self, user_data):
        yuki_type, yuki_key, yuki_value = user_data
        if yuki_type == "player" and yuki_key == "software_volume":
            self._refresh_software_volume()

    def __init__(self, parent):
        self._parent = parent
        self._refresh_software_volume()
        self._raise("delta > register settings object", self)
