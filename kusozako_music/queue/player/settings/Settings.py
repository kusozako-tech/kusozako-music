
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SoftwareVolume import DeltaSoftwareVolume
from .Status import DeltaStatus


class EchoSettings:

    def __init__(self, parent):
        DeltaStatus(parent)
        DeltaSoftwareVolume(parent)
