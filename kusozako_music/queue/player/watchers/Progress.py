
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaProgress(DeltaEntity):

    def _idle(self, playbin):
        _, duration = playbin.query_duration(Gst.Format.TIME)
        _, position = playbin.query_position(Gst.Format.TIME)
        self._raise("delta > progress", (position, duration))

    def _timeout(self, playbin):
        # to avoid widget redraw interupt. because...
        # GLib.timeout_add uses GLib.PRIORITY_DEFAULT (= 0)
        # that priper than widget redraw (= 120)
        GLib.idle_add(self._idle, playbin)
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        playbin = self._enquiry("delta > playbin")
        GLib.timeout_add(200, self._timeout, playbin)
