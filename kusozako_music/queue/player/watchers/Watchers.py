
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Bus import DeltaBus
from .Progress import DeltaProgress


class EchoWatchers:

    def __init__(self, parent):
        DeltaBus(parent)
        DeltaProgress(parent)
