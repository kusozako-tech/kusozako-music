
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaBus(DeltaEntity):

    def _on_state_changed(self, gst_bus, gst_message):
        _, new_status, _ = gst_message.parse_state_changed()
        data = QueueSignals.PLAYER_STATUS_CHANGED, new_status
        self._raise("delta > queue signal", data)

    def _on_eos(self, *args):
        data = QueueSignals.SHIFT_CURRENT_ROW, 1
        self._raise("delta > queue signal", data)

    def __init__(self, parent):
        self._parent = parent
        playbin = self._enquiry("delta > playbin")
        bus = playbin.get_bus()
        bus.add_signal_watch()
        bus.connect("message::eos", self._on_eos)
        bus.connect('message::state-changed', self._on_state_changed)
