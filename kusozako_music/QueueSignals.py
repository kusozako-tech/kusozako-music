
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

CURRENT_ROW_CHANGED = 0             # tree_row
SHIFT_CURRENT_ROW = 1               # (1 or -1) as tree_path index increment
PLAYER_STATUS_CHANGED = 2           # unused ?
PLAYER_POSITION_UPDATED = 3         # (float, float) as (position, duration)
PLAYER_POSITION_SEEK = 4
PLAYER_SEEK_FORWARD = 5
PLAYER_SEEK_REWIND = 6
QUEUE_METADATA_READING = 7
QUEUE_REALIZED = 8
QUEUE_CURRENT_ROW_REFRESHED = 9     # tree_row to refresh
QUEUE_EDIT_METADATA = 10            # tree_row to edit
QUEUE_FILTER_CHANGED = 11           # str as filter text
QUEUE_SORT_COLUMN_CHANGED = 12      # int as colums_id
QUEUE_SORT_ORDER_CHANGED = 13       # int as Gtk.SortType
VIEWERS_CONTENT_AREA_REALIZED = 14  # None
VIEWERS_POPUP_ITEM_POPUP = 15       # tree_row, widget, x as int, y as int
VIEWERS_POPUP_NON_ITEM_POPUP = 16   # widget, x as int, y as int

NUMBER_OF_SIGNALS = 17
