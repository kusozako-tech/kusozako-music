
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music import PlayerStatus
from kusozako_music import ShuffleMode

SOFTWARE_VOLUME = "player", "software_volume", 0.25
PLAYER_STATUS = "player", "status", PlayerStatus.PAUSED
SHUFFLE_MODE = "player", "shuffle_mode", ShuffleMode.REPEAT_ALL
