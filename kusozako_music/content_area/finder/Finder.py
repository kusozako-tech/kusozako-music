
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3 import ApplicationSignals
from kusozako_music import QueueSignals


class DeltaFinder(Gtk.Revealer, DeltaEntity):

    def _on_changed(self, search_entry):
        param = QueueSignals.QUEUE_FILTER_CHANGED, search_entry.get_text()
        self._raise("delta > queue signal", param)

    def _set_revealed(self):
        revealed = not self.get_child_revealed()
        self.set_reveal_child(revealed)
        if revealed:
            self._search_entry.grab_focus()
        else:
            self._search_entry.set_text("")

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == ApplicationSignals.FINDER_TOGGLE:
            self._set_revealed()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_reveal_child(False)
        self._search_entry = Gtk.SearchEntry()
        self._search_entry.set_margin_bottom(Unit(1))
        self._search_entry.connect("changed", self._on_changed)
        self.add(self._search_entry)
        self._raise("delta > add to container", self)
        self._raise("delta > register action object", self)
