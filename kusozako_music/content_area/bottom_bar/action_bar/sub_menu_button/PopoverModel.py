
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music import ViewersPageNames as PageNames


PAGES_MODEL = {
    "items": [
        {
            "type": "icon-button",
            "icon-name": "dialog-information-symbolic",
            "tooltip-text": _("Information"),
            "message": "delta > settings",
            "user-data": ("view", "main_stack_page", PageNames.INFO_VIEW),
            "close-on-clicked": False,
        },
        {
            "type": "icon-button",
            "icon-name": "view-grid-symbolic",
            "tooltip-text": _("Icon View"),
            "message": "delta > settings",
            "user-data": ("view", "main_stack_page", PageNames.ICON_VIEW),
            "close-on-clicked": False,
        },
        {
            "type": "icon-button",
            "icon-name": "view-list-symbolic",
            "tooltip-text": _("List View"),
            "message": "delta > settings",
            "user-data": ("view", "main_stack_page", PageNames.LIST_VIEW),
            "close-on-clicked": False,
        }
    ]
}

MAIN_PAGE_MODEL = {
    "page-name": "main",
    "items": [
        {
            "type": "horizontal-box",
            "model": PAGES_MODEL
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Edit Metadata"),
            "message": "delta > edit metadata",
            "user-data": None,
            "close-on-clicked": True
        }
    ]
}

POPOVER_MODEL = [MAIN_PAGE_MODEL]
