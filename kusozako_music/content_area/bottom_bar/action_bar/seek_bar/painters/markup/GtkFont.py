
from gi.repository import Gtk
from gi.repository import Pango


def get(font_size=0, weight=400):
    settings = Gtk.Settings.get_default()
    font_name = settings.get_property("gtk-font-name")
    font_description = Pango.font_description_from_string(font_name)
    size = font_description.get_size()/Pango.SCALE
    font_description.set_size((size+font_size)*Pango.SCALE)
    font_description.set_weight(weight)
    return font_description
