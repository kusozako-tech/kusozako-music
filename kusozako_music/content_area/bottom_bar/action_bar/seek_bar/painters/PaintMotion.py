
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaPaintMotion(DeltaEntity):

    def paint(self, cairo_context):
        pointed = self._enquiry("delta > pointed position")
        if pointed is None:
            return
        size = self._enquiry("delta > allocated size")
        rgba = self._enquiry("delta > rgba")
        red, green, blue, alpha = rgba
        new_rgba = red, green, blue, alpha*0.5
        cairo_context.set_source_rgba(*new_rgba)
        cairo_context.rectangle(0, 0, size.width * pointed, size.height)
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
