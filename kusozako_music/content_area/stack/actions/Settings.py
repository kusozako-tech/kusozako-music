
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaSettings(DeltaEntity):

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "view" and key == "main_stack_page":
            stack = self._enquiry("delta > stack")
            stack.set_visible_child_name(value)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register settings object", self)
