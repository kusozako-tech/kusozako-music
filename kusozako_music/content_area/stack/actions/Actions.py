
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Map import DeltaMap
from .Settings import DeltaSettings
from .FinderToggled import DeltaFinderToggled


class EchoActions:

    def __init__(self, parent):
        DeltaMap(parent)
        DeltaSettings(parent)
        DeltaFinderToggled(parent)
