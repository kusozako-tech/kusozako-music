
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaMap(DeltaEntity):

    def _on_map(self, stack):
        param = QueueSignals.VIEWERS_CONTENT_AREA_REALIZED, None
        self._raise("delta > queue signal", param)

    def __init__(self, parent):
        self._parent = parent
        stack = self._enquiry("delta > stack")
        stack.connect("map", self._on_map)
