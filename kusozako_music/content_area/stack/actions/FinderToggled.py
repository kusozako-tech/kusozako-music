
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3 import ApplicationSignals


class DeltaFinderToggled(DeltaEntity):

    def _try_move_to(self, stack):
        self._previous_page = stack.get_visible_child_name()
        if self._previous_page == "info-view":
            stack.set_visible_child_name("icon-view")

    def _back_to_previous_page(self, stack):
        stack.set_visible_child_name(self._previous_page)
        self._previous_page = None

    def _dispatch(self):
        stack = self._enquiry("delta > stack")
        if self._previous_page is None:
            self._try_move_to(stack)
        else:
            self._back_to_previous_page(stack)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == ApplicationSignals.FINDER_TOGGLE:
            self._dispatch()

    def __init__(self, parent):
        self._parent = parent
        self._previous_page = None
        self._raise("delta > register action object", self)
