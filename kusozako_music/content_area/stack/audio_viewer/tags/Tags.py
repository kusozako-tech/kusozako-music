
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Metadata import FoxtrotMetadata
from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes

ACCEPTABLE_SIGNALS = (
    QueueSignals.CURRENT_ROW_CHANGED,
    QueueSignals.QUEUE_CURRENT_ROW_REFRESHED
    )


class DeltaTags(Gtk.Label, DeltaEntity):

    def reset_orientation(self):
        if self._enquiry("delta > is vertically long"):
            self.set_margin_start(Unit(2))
            self.set_margin_end(Unit(2))
            self.set_xalign(0.5)
        else:
            self.set_margin_start(0)
            self.set_margin_end(Unit(2))
            self.set_xalign(0)

    def receive_transmission(self, user_data):
        signal, tree_row = user_data
        if signal not in ACCEPTABLE_SIGNALS:
            return
        gio_file = tree_row[ColumnTypes.GIO_FILE]
        meta_tags = self._metadata.get_from_uri(gio_file)
        template = _("Title:\n{}\n\nArtist:\n{}\n\nAlbum:\n{}")
        formated_text = template.format(*meta_tags)
        markup = GLib.markup_escape_text(formated_text)
        self.set_markup(markup)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            "",
            vexpand=True,
            wrap=True,
            selectable=True
            )
        self.set_xalign(0)
        self.set_use_markup(True)
        self._metadata = FoxtrotMetadata()
        self._raise("delta > add to container", self)
        self._raise("delta > register queue object", self)
