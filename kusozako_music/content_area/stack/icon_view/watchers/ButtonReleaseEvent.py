
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaButtonReleaseEvent(DeltaEntity):

    def _dispatch_event(self, icon_view, event):
        tree_path = icon_view.get_path_at_pos(event.x, event.y)
        v_adjustment = icon_view.get_vadjustment()
        adjusted_y = event.y-v_adjustment.get_value()
        if tree_path is None:
            signal_param = icon_view, event.x, adjusted_y
            param = QueueSignals.VIEWERS_POPUP_NON_ITEM_POPUP, signal_param
        else:
            model = icon_view.get_model()
            tree_row = model[tree_path]
            signal_param = tree_row, icon_view, event.x, adjusted_y
            param = QueueSignals.VIEWERS_POPUP_ITEM_POPUP, signal_param
        self._raise("delta > queue signal", param)

    def _on_button_release(self, icon_view, event):
        if event.button == 3:
            self._dispatch_event(icon_view, event)

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect("button-release-event", self._on_button_release)
