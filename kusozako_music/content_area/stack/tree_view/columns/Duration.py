
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from kusozako_music import ColumnTypes
from .ExtraFuncs import BravoExtraFuncs


class DeltaDuration(Gtk.TreeViewColumn, BravoExtraFuncs, DeltaEntity):

    def _construct(self):
        renderer = Gtk.CellRendererText(
            ellipsize=Pango.EllipsizeMode.END,
            xalign=1
            )
        Gtk.TreeViewColumn.__init__(
            self,
            title=_("Duration"),
            cell_renderer=renderer,
            text=ColumnTypes.DURATION_READABLE
            )
        self._set_sort_column(ColumnTypes.DURATION)
        self.set_resizable(False)
        self.set_expand(False)
        self._set_stripes(renderer)
        self._raise("delta > append tree view column", self)

    def __init__(self, parent):
        self._parent = parent
        self._construct()
