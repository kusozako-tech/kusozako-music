
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music import ColumnTypes
from .StatusIcon import DeltaStatusIcon
from .TextColumn import DeltaTextColumn
from .Duration import DeltaDuration


class EchoColumns:

    def __init__(self, parent):
        DeltaStatusIcon(parent)
        DeltaTextColumn.new(parent, _("Title"), ColumnTypes.TITLE)
        DeltaTextColumn.new(parent, _("Artist"), ColumnTypes.ARTIST)
        DeltaTextColumn.new(parent, _("Album"), ColumnTypes.ALBUM)
        DeltaDuration(parent)
