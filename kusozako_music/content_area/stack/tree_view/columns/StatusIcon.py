
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_music import ColumnTypes
from .ExtraFuncs import BravoExtraFuncs


class DeltaStatusIcon(Gtk.TreeViewColumn, DeltaEntity, BravoExtraFuncs):

    def _extra_data_func(self, renderer, model, tree_iter):
        renderer.set_property("height", Unit(4))
        tree_row = model[tree_iter]
        now_playing = tree_row[ColumnTypes.NOW_PLAYING]
        icon_name = "media-playback-start-symbolic" if now_playing else None
        renderer.set_property("icon-name", icon_name)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(
            self,
            title="",
            cell_renderer=renderer
            )
        self.set_sort_column_id(ColumnTypes.UNIQUE_ID)
        self.set_expand(False)
        self._set_stripes(renderer)
        self._raise("delta > append tree view column", self)
