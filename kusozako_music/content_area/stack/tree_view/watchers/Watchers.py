
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CurrentRowChanged import DeltaCurrentRowChanged
from .ButtonReleaseEvent import DeltaButtonReleaseEvent
from .QueueTooltip import DeltaQueryTooltip


class EchoWatchers:

    def __init__(self, parent):
        DeltaCurrentRowChanged(parent)
        DeltaButtonReleaseEvent(parent)
        DeltaQueryTooltip(parent)
