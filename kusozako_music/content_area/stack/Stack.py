
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .actions.Actions import EchoActions
from .audio_viewer.AudioViewer import DeltaAudioViewer
from .icon_view.IconView import DeltaIconView
from .tree_view.TreeView import DeltaTreeView


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_info_stack(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            vexpand=True,
            transition_type=Gtk.StackTransitionType.OVER_RIGHT_LEFT
            )
        DeltaAudioViewer(self)
        DeltaIconView(self)
        DeltaTreeView(self)
        self._raise("delta > add to container", self)
        EchoActions(self)
