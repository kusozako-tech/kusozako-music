
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.finder.Finder import DeltaFinder
from .finder.Finder import DeltaFinder as DeltaSearchBar
from .stack.Stack import DeltaStack
from .bottom_bar.BottomBar import DeltaBottomBar


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            vexpand=True
            )
        DeltaFinder(self)
        DeltaSearchBar(self)
        DeltaStack(self)
        DeltaBottomBar(self)
        self._raise("delta > add to container", self)
