
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.audio_metadata.AudioMetadata import FoxtrotAudioMetadata
from . import MTime
from . import Duration


class FoxtrotKeyFile:

    def _new(self, uri, md5_checksum, thumbnail_path, metadata_path):
        key_file = GLib.KeyFile.new()
        key_file.set_string("metadata", "uri", uri)
        key_file.set_string("metadata", "md5_checksum", md5_checksum)
        key_file.set_string("metadata", "thumbnail_status", "unknown")
        key_file.set_string("metadata", "thumbnail_path", thumbnail_path)
        key_file.set_string("metadata", "metadata_path", metadata_path)
        title, artist, album = self._metadata.get_from_uri(uri, "")
        if title == "":
            gio_file = Gio.File.new_for_uri(uri)
            title = gio_file.get_basename()
        key_file.set_string("metadata", "title", title)
        key_file.set_string("metadata", "artist", artist)
        key_file.set_string("metadata", "album", album)
        duration, duration_readable = Duration.from_uri(uri)
        key_file.set_uint64("metadata", "duration", duration)
        key_file.set_string("metadata", "duration_readable", duration_readable)
        key_file.save_to_file(metadata_path)
        return key_file

    def _load(self, metadata_path):
        key_file = GLib.KeyFile.new()
        key_file.load_from_file(metadata_path, GLib.KeyFileFlags.NONE)
        return key_file

    def new(self, uri, md5_checksum, thumbnail_path, metadata_path):
        if GLib.file_test(metadata_path, GLib.FileTest.EXISTS):
            if not MTime.older_than(uri, GLib.filename_to_uri(metadata_path)):
                return self._load(metadata_path)
        return self._new(uri, md5_checksum, thumbnail_path, metadata_path)

    def __init__(self):
        self._metadata = FoxtrotAudioMetadata()
