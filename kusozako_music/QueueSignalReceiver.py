
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaQueueSignalReceiver(DeltaEntity):

    SIGNAL = "define signal to handle here."
    REGISTRATION = "delta > register queue object"

    def _control(self, user_data=None):
        raise NotImplementedError

    def _on_initialize(self):
        # for optional instances
        pass

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.SIGNAL:
            self._control(param)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._raise(self.REGISTRATION, self)
