
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GdkPixbuf

NOW_PLAYING = 0
UNIQUE_ID = 1
GIO_FILE = 2
COVERART_ORIGNAL = 3
COVERART_TILE = 4
COVERART_SOURCE = 5
TITLE = 6
ARTIST = 7
ALBUM = 8
DURATION = 9
DURATION_READABLE = 10
TOOLTIP_TEXT = 11
NUMBER_OF_COLUMNS = 12

TYPES = (
    bool,               # 0: NOW_PLAYING
    int,                # 1: UNIQUE_ID
    Gio.File,           # 2: GIO_FILE
    GdkPixbuf.Pixbuf,   # 3: COVERART_ORIGINAL
    GdkPixbuf.Pixbuf,   # 4: COVERART_TILE
    GdkPixbuf.Pixbuf,   # 5: COVERART_SOURCE
    str,                # 6: TITLE
    str,                # 7: ARTIST
    str,                # 8: ALBUM
    int,                # 9: DURTION in seconds
    str,                # 10: DURATION_READABLE
    str,                # 11: TOOLTIP_TEXT
    )
