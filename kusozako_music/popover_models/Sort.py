
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SortColumnId import SORT_COLUMN_ID
from .SortOrder import SORT_ORDER

SORT = {
    "page-name": "sort",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        *SORT_COLUMN_ID,
        {
            "type": "separator"
        },
        *SORT_ORDER
    ]
}
