
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ViewerSwitcher import VIEWER_SWITCHER
from .Sort import SORT

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "horizontal-box",
            "model": VIEWER_SWITCHER
        },
        {
            "type": "separator"
        },
        {
            "type": "switcher",
            "title": _("Sort"),
            "message": "delta > switch stack to",
            "user-data": "sort",
        },
        {
            "type": "simple-action",
            "title": _("Edit Metadata"),
            "message": "delta > edit metadata",
            "user-data": None,
            "close-on-clicked": True
        }
    ]
}

NON_ITEM_POPUP = [MAIN_PAGE, SORT]
