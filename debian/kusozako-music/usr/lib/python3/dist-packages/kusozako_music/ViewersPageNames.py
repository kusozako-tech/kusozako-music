
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

INFO_VIEW = "info-view"
ICON_VIEW = "icon-view"
LIST_VIEW = "list-view"
