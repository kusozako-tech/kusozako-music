
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes

KEYS = (ColumnTypes.TITLE, ColumnTypes.ARTIST, ColumnTypes.ALBUM)


class DeltaFilter(DeltaEntity):

    def _compare(self, tree_row):
        for key in KEYS:
            data = GLib.utf8_strdown(tree_row[key], -1)
            if self._filter in data:
                return True
        return False

    def is_visible(self, tree_row):
        if self._filter == "":
            return True
        return self._compare(tree_row)

    def receive_transmission(self, user_data):
        signal, filter_text = user_data
        if signal != QueueSignals.QUEUE_FILTER_CHANGED:
            return
        self._filter = GLib.utf8_strdown(filter_text, -1)
        filter_model = self._enquiry("delta > filter model")
        filter_model.refilter()

    def __init__(self, parent):
        self._parent = parent
        self._filter = ""
        self._raise("delta > register queue object", self)
