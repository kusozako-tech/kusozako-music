
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.metadata_editor.MetadataEditor import DeltaMetadataEditor
from kusozako_music.metadata.Metadata import FoxtrotMetadata
from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver
from kusozako_music.thumbnail.Thumbnail import DeltaThumbnail
from .header_tag.HeaderTag import FoxtrotHeaderTag

TOOLTIP_TEMPLATE = "{}\n\n{}\n{}"


class DeltaQueueEditMetadata(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.QUEUE_EDIT_METADATA

    def _try_set_now_playing_header(self, tree_row):
        if not tree_row[ColumnTypes.NOW_PLAYING]:
            return
        self._header_tag.set_to_tree_row(tree_row)
        data = QueueSignals.QUEUE_CURRENT_ROW_REFRESHED, tree_row
        self._raise("delta > queue signal", data)

    def _edit_tree_row(self, tree_row, gio_file):
        uri = gio_file.get_uri()
        key_file = self._metadata.from_uri(uri)
        title = key_file.get_string("metadata", "title")
        artist = key_file.get_string("metadata", "artist")
        album = key_file.get_string("metadata", "album")
        tooltip_text = TOOLTIP_TEMPLATE.format(title, artist, album)
        pixbufs = self._thumbnail.get_for_file_and_title(gio_file, title)
        original, pixbuf = pixbufs
        tree_row[ColumnTypes.TITLE] = title
        tree_row[ColumnTypes.ARTIST] = artist
        tree_row[ColumnTypes.ALBUM] = album
        tree_row[ColumnTypes.COVERART_ORIGNAL] = original
        tree_row[ColumnTypes.COVERART_TILE] = pixbuf
        tree_row[ColumnTypes.COVERART_SOURCE] = pixbuf.copy()
        tree_row[ColumnTypes.TOOLTIP_TEXT] =tooltip_text
        self._try_set_now_playing_header(tree_row)

    def _control(self, tree_row):
        gio_file = tree_row[ColumnTypes.GIO_FILE]
        response = DeltaMetadataEditor.new_for_gio_file(self, gio_file)
        if response:
            self._edit_tree_row(tree_row, gio_file)

    def _on_initialize(self):
        self._metadata = FoxtrotMetadata()
        self._thumbnail = DeltaThumbnail(self)
        self._header_tag = FoxtrotHeaderTag()
