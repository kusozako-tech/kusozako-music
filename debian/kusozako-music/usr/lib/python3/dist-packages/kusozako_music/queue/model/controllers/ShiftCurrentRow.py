
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes
from kusozako_music import DefaultSettings
from kusozako_music import ShuffleMode
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver


class DeltaShiftCurrentRow(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.SHIFT_CURRENT_ROW

    def _get_current_tree_path(self, model):
        for tree_row in model:
            if tree_row[ColumnTypes.NOW_PLAYING]:
                return int(tree_row.path.to_string())
        return None

    def _random(self, model):
        path = GLib.random_int_range(0, len(model))
        data = QueueSignals.CURRENT_ROW_CHANGED, model[path]
        self._raise("delta > queue signal", data)

    def _shift(self, model, shift):
        tree_path = self._get_current_tree_path(model)
        if tree_path is None:
            return
        path = (tree_path+shift) % len(model)
        data = QueueSignals.CURRENT_ROW_CHANGED, model[path]
        self._raise("delta > queue signal", data)

    def _control(self, shift):
        model = self._enquiry("delta > model")
        query = "delta > settings", DefaultSettings.SHUFFLE_MODE
        shuffle_mode = self._enquiry(*query)
        if shuffle_mode == ShuffleMode.SHUFFLE:
            self._random(model)
        elif shuffle_mode == ShuffleMode.REPEAT_ONE:
            data = QueueSignals.PLAYER_POSITION_SEEK, 0
            self._raise("delta > queue signal", data)
        else:
            self._shift(model, shift)
