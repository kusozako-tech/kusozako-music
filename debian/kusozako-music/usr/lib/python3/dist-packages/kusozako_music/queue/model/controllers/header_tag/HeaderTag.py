
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import math
import cairo
from gi.repository import Gdk
from gi.repository import PangoCairo
from . import LayoutFactory
from libkusozako3.Ux import Unit
from kusozako_music import ColumnTypes
from .TriangleShade import FoxtrotTriagleShade

SIZE = Unit(16), Unit(16)
HEIGHT = Unit(16)*0.6
OFFSET_RATE = 1/math.sqrt(2)
ANGLE = -45
SHADE_COLOR_RGBA = 255/255, 165/255, 0/255, 0.9


class FoxtrotHeaderTag:

    def _get_offset_positions(self, layout_height):
        offset = layout_height*OFFSET_RATE
        return 0-offset, HEIGHT-offset

    def _paint_text(self, cairo_context, markup_):
        layout = LayoutFactory.get_triangle(cairo_context)
        layout.set_markup(markup_)
        _, height = layout.get_pixel_size()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(*self._get_offset_positions(height))
        cairo_context.save()
        cairo_context.rotate(math.radians(ANGLE))
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)
        cairo_context.restore()

    def set_to_tree_row(self, tree_row):
        pixbuf = tree_row[ColumnTypes.COVERART_SOURCE]
        surface = cairo.ImageSurface(cairo.Format.ARGB32, *SIZE)
        cairo_context = cairo.Context(surface)
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, 0, 0)
        cairo_context.paint()
        self._shade.north_west(cairo_context, SHADE_COLOR_RGBA)
        self._paint_text(cairo_context, "NOW\nPLAYING")
        new_pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, *SIZE)
        tree_row[ColumnTypes.COVERART_TILE] = new_pixbuf.copy()

    def __init__(self):
        self._shade = FoxtrotTriagleShade()
