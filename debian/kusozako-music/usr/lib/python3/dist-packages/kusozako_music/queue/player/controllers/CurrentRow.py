
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes
from kusozako_music import DefaultSettings
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver


class DeltaCurrentRow(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.CURRENT_ROW_CHANGED

    def _control(self, tree_row):
        gio_file = tree_row[ColumnTypes.GIO_FILE]
        self._playbin = self._enquiry("delta > playbin")
        status_hook = self._enquiry(
            "delta > settings",
            DefaultSettings.PLAYER_STATUS
            )
        self._playbin.set_state(Gst.State.NULL)
        self._playbin.set_property("uri", gio_file.get_uri())
        self._playbin.set_state(status_hook)
