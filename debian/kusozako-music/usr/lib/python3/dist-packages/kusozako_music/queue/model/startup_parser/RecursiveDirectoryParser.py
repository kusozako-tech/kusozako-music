
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.mime_type import MimeType
from .FileEnumerator import DeltaFileEnumerator


class DeltaRecursiveDirectoryParser(DeltaEntity):

    def _delta_call_break(self, directory):
        self._directories.remove(directory)
        if not self._directories:
            self._raise("delta > model realized")

    def _delta_call_continue(self, user_data):
        gio_file, file_info = user_data
        mime_type = MimeType.from_file_info(gio_file, file_info)
        if mime_type in ("audio/mpeg", "audio/x-m4a"):
            self._raise("delta > file found", gio_file)
        elif mime_type == "inode/directory":
            directory = gio_file.get_path()
            self._directories.append(directory)
            self._file_enumerator.parse_directory_async(directory)

    def parse_async(self, directory):
        self._directories.append(directory)
        self._file_enumerator.parse_directory_async(directory)

    def __init__(self, parent):
        self._parent = parent
        self._directories = []
        self._file_enumerator = DeltaFileEnumerator(self)
