
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music.thumbnail.Thumbnail import DeltaThumbnail
from kusozako_music.metadata.Metadata import FoxtrotMetadata

TOOLTIP_TEMPLATE = "{}\n\n{}\n{}"


class DeltaMetadata(DeltaEntity):

    def read_metadata(self, gio_file):
        uri = gio_file.get_uri()
        key_file = self._metadata.from_uri(uri)
        title = key_file.get_string("metadata", "title")
        artist = key_file.get_string("metadata", "artist")
        album = key_file.get_string("metadata", "album")
        pixbufs = self._thumbnail.get_for_file_and_title(gio_file, title)
        original, pixbuf = pixbufs
        source_pixbuf = pixbuf.copy()
        row_data = (
            False,
            self._unique_id,
            gio_file,
            original,
            pixbuf,
            source_pixbuf,
            title,
            artist,
            album,
            key_file.get_uint64("metadata", "duration"),
            key_file.get_string("metadata", "duration_readable"),
            TOOLTIP_TEMPLATE.format(title, artist, album),
            )
        self._unique_id += 1
        self._raise("delta > append row", row_data)

    def __init__(self, parent):
        self._parent = parent
        self._metadata = FoxtrotMetadata.get_default()
        self._unique_id = 0
        self._thumbnail = DeltaThumbnail(self)
