
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from kusozako_music.popover_models import PopoverModels
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver
from kusozako_music import QueueSignals


class DeltaNonItemPopup(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.VIEWERS_POPUP_NON_ITEM_POPUP

    def _control(self, param):
        widget, x, y = param
        self._popover_menu.popup_for_position(widget, x, y)

    def _on_initialize(self):
        self._popover_menu = DeltaPopoverMenu.new_for_model(
            self,
            PopoverModels.NON_ITEM_POPUP
            )
