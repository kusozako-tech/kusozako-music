
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ItemPopup import DeltaItemPopup
from .NonItemPopup import DeltaNonItemPopup


class EchoViewersPopup:

    def __init__(self, parent):
        DeltaItemPopup(parent)
        DeltaNonItemPopup(parent)
