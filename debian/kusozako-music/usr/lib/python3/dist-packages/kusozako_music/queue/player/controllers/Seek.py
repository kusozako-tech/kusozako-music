
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako_music import QueueSignals
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver


class DeltaSeek(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.PLAYER_POSITION_SEEK

    def _control(self, rate):
        playbin = self._enquiry("delta > playbin")
        _, duration = playbin.query_duration(Gst.Format.TIME)
        playbin.seek_simple(
            Gst.Format.TIME,
            Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
            duration*rate
            )
