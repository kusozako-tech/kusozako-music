
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import DefaultSettings


class DeltaStatus(DeltaEntity):

    def _reset_settings(self):
        status = self._enquiry(
            "delta > settings",
            DefaultSettings.PLAYER_STATUS
            )
        playbin = self._enquiry("delta > playbin")
        playbin.set_state(status)

    def receive_transmission(self, user_data):
        yuki_type, yuki_key, yuki_value = user_data
        if yuki_type == "player" and yuki_key == "status":
            self._reset_settings()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register settings object", self)
