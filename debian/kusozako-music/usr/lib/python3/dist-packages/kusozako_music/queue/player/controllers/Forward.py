
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako_music import QueueSignals
from kusozako_music import ShuffleMode
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver

FIVE_SECONDS_IN_GST_FORMAT = 5*1000*1000*1000
SEEK_FLAGS = Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT


class DeltaForward(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.PLAYER_SEEK_FORWARD

    def _do_not_shift(self):
        query = "player", "shuffle_mode"
        settings = self._enquiry("delta > settings", query)
        return settings == ShuffleMode.REPEAT_ONE

    def _control(self, param=None):
        if self._do_not_shift():
            playbin = self._enquiry("delta > playbin")
            playbin.seek_simple(Gst.Format.TIME, SEEK_FLAGS, 0)
        else:
            data = QueueSignals.SHIFT_CURRENT_ROW, 1
            self._raise("delta > queue signal", data)
