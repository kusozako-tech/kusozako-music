
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals
from .actions.Actions import EchoActions
from .model.Model import DeltaModel
from .player.Player import DeltaPlayer
from .Filter import DeltaFilter


class DeltaQueue(DeltaEntity):

    def _delta_info_model(self):
        return self._model

    def _delta_info_filter_model(self):
        return self._filter_model

    def _delta_call_model_realized(self):
        self._player.initialize_controllers()
        self._model.initialize_controllers()
        param = QueueSignals.QUEUE_REALIZED, None
        self._raise("delta > queue signal", param)

    def _visible_func(self, model, tree_iter, user_data=None):
        return self._filter.is_visible(model[tree_iter])

    def get_filter_model(self):
        return self._filter_model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        self._filter_model = self._model.filter_new()
        self._filter_model.set_visible_func(self._visible_func)
        self._filter = DeltaFilter(self)
        EchoActions(self)
        self._player = DeltaPlayer(self)
