
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes
from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver
from .header_tag.HeaderTag import FoxtrotHeaderTag


class DeltaCurrentRow(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.CURRENT_ROW_CHANGED

    def _set_to_not_now_playing(self, tree_row):
        tree_row[ColumnTypes.NOW_PLAYING] = False
        pixbuf = tree_row[ColumnTypes.COVERART_SOURCE]
        tree_row[ColumnTypes.COVERART_TILE] = pixbuf.copy()

    def _set_to_now_playing(self, tree_row):
        tree_row[ColumnTypes.NOW_PLAYING] = True
        self._header_tag.set_to_tree_row(tree_row)
        title = tree_row[ColumnTypes.TITLE]
        self._raise("delta > application window title", title)

    def _control(self, tree_row):
        model = self._enquiry("delta > model")
        for iter_ in model:
            if iter_[ColumnTypes.NOW_PLAYING]:
                self._set_to_not_now_playing(iter_)
        self._set_to_now_playing(tree_row)

    def _on_initialize(self):
        self._header_tag = FoxtrotHeaderTag()
