
from libkusozako3.Ux import Unit

SIZE_04 = Unit(16)*0.4
SIZE_06 = Unit(16)*0.6
SIZE_10 = Unit(16)


class FoxtrotTriagleShade:

    def north_east(self, cairo_context, color):
        cairo_context.set_source_rgba(*color)
        cairo_context.move_to(SIZE_10, 0)
        cairo_context.line_to(SIZE_10, SIZE_06)
        cairo_context.line_to(SIZE_04, 0)
        cairo_context.fill()

    def north_west(self, cairo_context, color):
        cairo_context.set_source_rgba(*color)
        cairo_context.move_to(0, 0)
        cairo_context.line_to(SIZE_06, 0)
        cairo_context.line_to(0, SIZE_06)
        cairo_context.fill()
