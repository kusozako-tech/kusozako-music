
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .RecursiveDirectoryParser import DeltaRecursiveDirectoryParser
from .actions.Actions import EchoActions


class DeltaStartupParser(DeltaEntity):

    def _delta_call_request_parse_directory(self, directory):
        self._recursive_directory_parser.parse_async(directory)

    def __init__(self, parent):
        self._parent = parent
        self._recursive_directory_parser = DeltaRecursiveDirectoryParser(self)
        EchoActions(self)
