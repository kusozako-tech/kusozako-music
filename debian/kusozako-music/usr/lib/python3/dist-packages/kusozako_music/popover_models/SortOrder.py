
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_music import QueueSignals

ORDER_CHANGED = QueueSignals.QUEUE_SORT_ORDER_CHANGED


SORT_ORDER = (
    {
        "type": "check-action",
        "title": _("A…Z"),
        "message": "delta > queue signal",
        "user-data": (ORDER_CHANGED, Gtk.SortType.ASCENDING),
        "close-on-clicked": False,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": Gtk.SortType.ASCENDING
    },
    {
        "type": "check-action",
        "title": _("Z…A"),
        "message": "delta > queue signal",
        "user-data": (ORDER_CHANGED, Gtk.SortType.DESCENDING),
        "close-on-clicked": False,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": Gtk.SortType.DESCENDING
    },
)
