
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes

COLUMN_CHANGED = QueueSignals.QUEUE_SORT_COLUMN_CHANGED


SORT_COLUMN_ID = (
    {
        "type": "check-action",
        "title": _("By Title"),
        "message": "delta > queue signal",
        "user-data": (COLUMN_CHANGED, ColumnTypes.TITLE),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": ColumnTypes.TITLE
    },
    {
        "type": "check-action",
        "title": _("By Artist"),
        "message": "delta > queue signal",
        "user-data": (COLUMN_CHANGED, ColumnTypes.ARTIST),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": ColumnTypes.ARTIST
    },
    {
        "type": "check-action",
        "title": _("By Album"),
        "message": "delta > queue signal",
        "user-data": (COLUMN_CHANGED, ColumnTypes.ALBUM),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": ColumnTypes.ALBUM
    },
)
