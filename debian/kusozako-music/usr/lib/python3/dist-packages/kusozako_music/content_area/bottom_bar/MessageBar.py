
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaMessageBar(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == QueueSignals.QUEUE_REALIZED:
            self._raise("delta > switch stack to", "action-bar")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, _("Please Wait ....."))
        self._raise("delta > add to stack named", (self, "message-bar"))
        self._raise("delta > register queue object", self)
