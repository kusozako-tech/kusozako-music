
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals
from kusozako_music import ViewersPageNames
from kusozako_music import ColumnTypes
from .columns.Columns import EchoColumns
from .watchers.Watchers import EchoWatchers


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_append_tree_view_column(self, tree_view_column):
        self.append_column(tree_view_column)

    def _delta_info_tree_view(self):
        return self

    def _on_row_activated(self, tree_view, tree_path, column):
        model = tree_view.get_model()
        data = QueueSignals.CURRENT_ROW_CHANGED, model[tree_path]
        self._raise("delta > queue signal", data)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.TreeView.__init__(self, model=self._enquiry("delta > model"))
        EchoColumns(self)
        EchoWatchers(self)
        scrolled_window.add(self)
        self.connect("row-activated", self._on_row_activated)
        user_data = scrolled_window, ViewersPageNames.LIST_VIEW
        self._raise("delta > add to stack named", user_data)
