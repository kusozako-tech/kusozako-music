
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals
from kusozako_music import PlayerStatus


class DeltaItemActivated(DeltaEntity):

    def _on_item_activated(self, icon_view, tree_path):
        model = icon_view.get_model()
        data = QueueSignals.CURRENT_ROW_CHANGED, model[tree_path]
        self._raise("delta > queue signal", data)
        data = "player", "status",  PlayerStatus.PLAYING
        self._raise("delta > settings", data)

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect("item-activated", self._on_item_activated)
