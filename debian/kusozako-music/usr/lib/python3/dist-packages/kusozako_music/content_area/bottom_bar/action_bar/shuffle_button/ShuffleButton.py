
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import ShuffleMode
from kusozako_music import QueueSignals
from .Image import DeltaImage


class DeltaShuffleButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        if self._message_data is None:
            return
        data = "player", "shuffle_mode", self._message_data
        self._raise("delta > settings", data)

    def _delta_call_shuffle_mode_changed(self, shuffle_mode):
        self._message_data = ShuffleMode.get_next_to(shuffle_mode)
        self.props.tooltip_text = ShuffleMode.get_tooltip_text(shuffle_mode)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == QueueSignals.QUEUE_REALIZED:
            self.set_image(DeltaImage(self))

    def __init__(self, parent):
        self._parent = parent
        self._message_data = None
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register queue object", self)
