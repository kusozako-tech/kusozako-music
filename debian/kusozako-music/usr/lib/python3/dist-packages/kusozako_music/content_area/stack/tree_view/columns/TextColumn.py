
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from .ExtraFuncs import BravoExtraFuncs


class DeltaTextColumn(Gtk.TreeViewColumn, BravoExtraFuncs, DeltaEntity):

    @classmethod
    def new(cls, parent, header_title, column_id):
        text_column = cls(parent)
        text_column.construct(header_title, column_id)

    def construct(self, header_title, column_id):
        renderer = Gtk.CellRendererText(ellipsize=Pango.EllipsizeMode.END)
        Gtk.TreeViewColumn.__init__(
            self,
            title=header_title,
            cell_renderer=renderer,
            text=column_id
            )
        self._set_sort_column(column_id)
        self.set_resizable(True)
        self.set_expand(True)
        self._set_stripes(renderer)
        self._raise("delta > append tree view column", self)

    def __init__(self, parent):
        self._parent = parent
