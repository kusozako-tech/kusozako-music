
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import ColumnTypes
from kusozako_music import QueueSignals
from kusozako_music import ViewersPageNames
from .watchers.Watchers import EchoWatchers


class DeltaIconView(Gtk.IconView, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, gio_file = user_data
        if signal != QueueSignals.CURRENT_ROW_CHANGED:
            return
        self.set_cursor(gio_file.path, None, False)

    def _delta_info_icon_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.IconView.__init__(
            self,
            model=self._enquiry("delta > model"),
            pixbuf_column=ColumnTypes.COVERART_TILE
            )
        scrolled_window.add(self)
        EchoWatchers(self)
        stacking_data = scrolled_window, ViewersPageNames.ICON_VIEW
        self._raise("delta > add to stack named", stacking_data)
        self._raise("delta > register queue object", self)
