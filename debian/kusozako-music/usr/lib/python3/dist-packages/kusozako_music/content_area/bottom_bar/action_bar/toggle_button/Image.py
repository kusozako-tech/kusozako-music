
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import DefaultSettings
from kusozako_music import PlayerStatus

START = "media-playback-start-symbolic"
PAUSE = "media-playback-pause-symbolic"


class DeltaImage(Gtk.Image, DeltaEntity):

    def _reset_settings(self):
        status = self._enquiry(
            "delta > settings",
            DefaultSettings.PLAYER_STATUS
            )
        icon_name = START if status == PlayerStatus.PAUSED else PAUSE
        self.set_from_icon_name(icon_name, Gtk.IconSize.SMALL_TOOLBAR)
        self._raise("delta > player status changed", status)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "player" and key == "status":
            self._reset_settings()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._reset_settings()
        self._raise("delta > register settings object", self)
