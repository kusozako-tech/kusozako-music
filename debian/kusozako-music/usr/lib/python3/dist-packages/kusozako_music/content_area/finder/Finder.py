
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_music import QueueSignals
from kusozako_music import ViewersPageNames


class DeltaFinder(Gtk.Revealer, DeltaEntity):

    def _on_changed(self, search_entry):
        query = search_entry.get_text()
        param = QueueSignals.QUEUE_FILTER_CHANGED, query
        self._raise("delta > queue signal", param)

    def _set_reveal_child(self, value):
        revealed = (value != ViewersPageNames.INFO_VIEW)
        self.set_reveal_child(revealed)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "view" and key == "main_stack_page":
            self._set_reveal_child(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_reveal_child(False)
        self._search_entry = Gtk.SearchEntry()
        self._search_entry.set_margin_bottom(Unit(1))
        self._search_entry.connect("changed", self._on_changed)
        self.add(self._search_entry)
        self._raise("delta > add to container", self)
        self._raise("delta > register settings object", self)
