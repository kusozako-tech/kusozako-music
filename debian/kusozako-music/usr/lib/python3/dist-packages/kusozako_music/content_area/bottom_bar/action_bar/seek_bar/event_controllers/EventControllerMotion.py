
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaEventControllerMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _on_motion(self, event_controller_motion, x, y):
        size = self._enquiry("delta > allocated size")
        self._raise("delta > pointer position", x/size.width)

    def _on_leave(self, *event_controller_motion):
        self._raise("delta > pointer position", None)

    def __init__(self, parent):
        self._parent = parent
        drawing_area = self._enquiry("delta > drawing area")
        Gtk.EventControllerMotion.__init__(self, widget=drawing_area)
        self.connect("motion", self._on_motion)
        drawing_area.connect("leave-notify-event", self._on_leave)
        # TODO: can't connect to leave event. WHY ?
        # self.connect("leave", self._on_leave)
