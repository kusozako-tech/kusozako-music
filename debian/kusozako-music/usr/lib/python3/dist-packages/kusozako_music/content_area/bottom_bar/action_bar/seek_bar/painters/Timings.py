
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaTimings(DeltaEntity):

    def receive_transmission(self, user_data):
        self._current = user_data
        self._raise("delta > queue draw")
        """
        signal, param = user_data
        if signal == QueueSignals.PLAYER_POSITION_UPDATED:
            self._current = param
            self._raise("delta > queue draw")
        """

    def __init__(self, parent):
        self._parent = parent
        self._current = 0, 1
        # self._raise("delta > register queue object", self)
        self._raise("delta > register progress object", self)

    @property
    def timing(self):
        return self._current
