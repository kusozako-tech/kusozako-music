
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.QueueSignalReceiver import AlfaQueueSignalReceiver
from kusozako_music import QueueSignals


class DeltaCurrentRowChanged(AlfaQueueSignalReceiver):

    SIGNAL = QueueSignals.CURRENT_ROW_CHANGED

    def _set_cursor(self, tree_path):
        tree_view = self._enquiry("delta > tree view")
        tree_view.set_cursor(tree_path, None, False)

    def _on_map(self, tree_view):
        if self._last_path is not None:
            self._set_cursor(self._last_path)

    def _control(self, tree_row):
        self._last_path = tree_row.path
        self._set_cursor(self._last_path)

    def _on_initialize(self):
        tree_view = self._enquiry("delta > tree view")
        self._last_path = None
        tree_view.connect("map", self._on_map)
