
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .sub_menu_button.SubMenuButton import DeltaSubMenuButton
from .RewindButton import DeltaRewindButton
from .toggle_button.ToggleButton import DeltaToggleButton
from .ForwardButton import DeltaForwardButton
from .seek_bar.SeekBar import DeltaSeekBar
from .shuffle_button.ShuffleButton import DeltaShuffleButton
from .volume_button.VolumeButton import DeltaVolumeButton


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaSubMenuButton(self)
        DeltaRewindButton(self)
        DeltaToggleButton(self)
        DeltaForwardButton(self)
        DeltaSeekBar(self)
        DeltaShuffleButton(self)
        DeltaVolumeButton(self)
        self._raise("delta > add to stack named", (self, "action-bar"))
