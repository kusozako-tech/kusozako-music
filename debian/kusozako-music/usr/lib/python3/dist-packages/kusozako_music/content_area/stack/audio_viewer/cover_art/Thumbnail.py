
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3.audio_thumbnail.AudioThumbnail import FoxtrotAudioThumbnail
from kusozako_music import QueueSignals
from kusozako_music import ColumnTypes

THUMBNAIL_SIZE = Unit(20)
ACCEPTABLE_SIGNALS = (
    QueueSignals.CURRENT_ROW_CHANGED,
    QueueSignals.QUEUE_CURRENT_ROW_REFRESHED
    )


class DeltaThumbnail(DeltaEntity):

    def get_pixbuf(self):
        return self._thumbnail

    def receive_transmission(self, user_data):
        signal, tree_row = user_data
        if signal not in ACCEPTABLE_SIGNALS:
            return
        gio_file = tree_row[ColumnTypes.GIO_FILE]
        uri = gio_file.get_uri()
        self._thumbnail = self._audio_thumbnail.get_for_uri(uri)
        self._raise("delta > queue draw")

    def __init__(self, parent):
        self._parent = parent
        self._audio_thumbnail = FoxtrotAudioThumbnail(THUMBNAIL_SIZE)
        self._thumbnail = None
        self._raise("delta > register queue object", self)
