
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ItemActivated import DeltaItemActivated
from .ButtonReleaseEvent import DeltaButtonReleaseEvent
from .QueryTooltip import DeltaQueryTooltip


class EchoWatchers:

    def __init__(self, parent):
        DeltaItemActivated(parent)
        DeltaButtonReleaseEvent(parent)
        DeltaQueryTooltip(parent)
