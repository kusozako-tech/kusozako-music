
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3 import ReadableTime


class DeltaMarkup(DeltaEntity):

    def _get_timings(self):
        current, duration = self._enquiry("delta > timing")
        pointed_position = self._enquiry("delta > pointed position")
        if pointed_position is not None:
            current = duration*pointed_position
        return current, duration

    def get_current(self):
        current, duration = self._get_timings()
        if 0 > duration:
            return _("pending...")
        current_readable = ReadableTime.from_nanoseconds(current)
        duration_readable = ReadableTime.from_nanoseconds(duration)
        return "{} / {}".format(current_readable, duration_readable)

    def __init__(self, parent):
        self._parent = parent
