
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaRewindButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        data = QueueSignals.PLAYER_SEEK_REWIND, None
        self._raise("delta > queue signal", data)

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(
            "media-seek-backward-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(
            self,
            image=image,
            relief=Gtk.ReliefStyle.NONE
            )
        self.props.tooltip_text = _("Rewind")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
