
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_music import ViewersPageNames
from .cover_art.CoverArt import DeltaCoverArt
from .tags.Tags import DeltaTags


class DeltaAudioViewer(Gtk.Box, DeltaEntity):

    def _get_orientation(self):
        rectangle, _ = self.get_allocated_size()
        if rectangle.width >= rectangle.height:
            return Gtk.Orientation.HORIZONTAL
        return Gtk.Orientation.VERTICAL

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_check_resized(self):
        orientation = self._get_orientation()
        self.set_orientation(orientation)
        self.set_homogeneous(orientation == Gtk.Orientation.HORIZONTAL)
        self._tags.reset_orientation()

    def _delta_info_is_vertically_long(self):
        return self._get_orientation() == Gtk.Orientation.VERTICAL

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_spacing(Unit(4))
        DeltaCoverArt(self)
        self._tags = DeltaTags(self)
        stacking_data = self, ViewersPageNames.INFO_VIEW
        self._raise("delta > add to stack named", stacking_data)
        self._raise("delta > css", (self, "primary-surface-color-class"))
