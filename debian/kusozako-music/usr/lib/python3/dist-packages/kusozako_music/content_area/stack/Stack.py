
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals
from .audio_viewer.AudioViewer import DeltaAudioViewer
from .icon_view.IconView import DeltaIconView
from .tree_view.TreeView import DeltaTreeView


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _on_map(self, stack):
        param = QueueSignals.VIEWERS_CONTENT_AREA_REALIZED, None
        self._raise("delta > queue signal", param)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "view" and key == "main_stack_page":
            self.set_visible_child_name(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            vexpand=True,
            transition_type=Gtk.StackTransitionType.OVER_RIGHT_LEFT
            )
        DeltaAudioViewer(self)
        DeltaIconView(self)
        DeltaTreeView(self)
        self._raise("delta > add to container", self)
        self.connect("map", self._on_map)
        self._raise("delta > register settings object", self)
