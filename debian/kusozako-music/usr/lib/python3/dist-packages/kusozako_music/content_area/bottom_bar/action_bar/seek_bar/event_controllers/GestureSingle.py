
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaGestureSingle(Gtk.GestureSingle, DeltaEntity):

    def _on_begin(self, gesture, event_sequence):
        _, x, _ = gesture.get_point(event_sequence)
        size = self._enquiry("delta > allocated size")
        data = QueueSignals.PLAYER_POSITION_SEEK, x/size.width
        self._raise("delta > queue signal", data)

    def __init__(self, parent):
        self._parent = parent
        drawing_area = self._enquiry("delta > drawing area")
        Gtk.GestureSingle.__init__(self, widget=drawing_area)
        self.connect("begin", self._on_begin)
