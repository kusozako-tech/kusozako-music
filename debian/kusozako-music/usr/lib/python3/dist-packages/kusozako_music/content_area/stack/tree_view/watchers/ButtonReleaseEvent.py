
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_music import QueueSignals


class DeltaButtonReleaseEvent(DeltaEntity):

    def _is_valid_input(self, tree_view, event):
        if event.button != 3:
            return False
        _, border = tree_view.get_border()
        if event.window.get_height() == border.top:
            return False
        return True

    def _get_tree_row(self, tree_view, event):
        response = tree_view.get_path_at_pos(event.x, event.y)
        if response is None:
            return None
        tree_path, _, _, _ = response
        model = tree_view.get_model()
        return model[tree_path]

    def _on_button_release(self, tree_view, event):
        if not self._is_valid_input(tree_view, event):
            return
        tree_row = self._get_tree_row(tree_view, event)
        if tree_row is None:
            return
        _, border = tree_view.get_border()
        signal_param = tree_row, tree_view, event.x, event.y+border.top
        param = QueueSignals.VIEWERS_POPUP_ITEM_POPUP, signal_param
        self._raise("delta > queue signal", param)

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.connect("button-release-event", self._on_button_release)
