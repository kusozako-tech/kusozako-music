
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from kusozako_music import QueueSignals
from .PopoverModel import POPOVER_MODEL

ICON_NAME = "view-more-symbolic"
ICON_SIZE = Gtk.IconSize.SMALL_TOOLBAR


class DeltaSubMenuButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button, popover):
        popover.popup_for_align(button, 0.5, 0.1)

    def _delta_call_edit_metadata(self):
        if self._current_row is None:
            return
        param = QueueSignals.QUEUE_EDIT_METADATA, self._current_row
        self._raise("delta > queue signal", param)

    def receive_transmission(self, user_data):
        signal, tree_row = user_data
        if signal != QueueSignals.CURRENT_ROW_CHANGED:
            return
        self._current_row = tree_row

    def __init__(self, parent):
        self._parent = parent
        self._current_row = None
        image = Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        popover = DeltaPopoverMenu.new_for_model(self, POPOVER_MODEL)
        self.connect("clicked", self._on_clicked, popover)
        self._raise("delta > add to container", self)
        self._raise("delta > register queue object", self)
