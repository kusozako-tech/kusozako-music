
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_music import ShuffleMode
from kusozako_music.DefaultSettings import SHUFFLE_MODE


class DeltaImage(Gtk.Image, DeltaEntity):

    def _refresh_settings(self, shuffle_mode=None):
        if shuffle_mode is None:
            shuffle_mode = self._enquiry("delta > settings", SHUFFLE_MODE)
        icon_name = ShuffleMode.ICON_NAMES[shuffle_mode]
        self.set_from_icon_name(icon_name, Gtk.IconSize.SMALL_TOOLBAR)
        self._raise("delta > shuffle mode changed", shuffle_mode)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "player" and key == "shuffle_mode":
            self._refresh_settings(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._refresh_settings()
        self._raise("delta > register settings object", self)
