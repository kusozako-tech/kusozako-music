
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

CACHE_DIR = GLib.get_user_cache_dir()


class FoxtrotCacheDirectory:

    def from_md5_checksum(self, md5_checksum):
        names = [self._directory, md5_checksum+self._extension]
        return GLib.build_filenamev(names)

    def __init__(self, directory, sub_directory, extension):
        self._extension = extension
        names = [CACHE_DIR, directory, sub_directory]
        self._directory = GLib.build_filenamev(names)
        if GLib.file_test(self._directory, GLib.FileTest.EXISTS):
            return
        gio_file = Gio.File.new_for_path(self._directory)
        gio_file.make_directory_with_parents()
