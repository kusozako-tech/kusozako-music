
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from .CacheDirectory import FoxtrotCacheDirectory


class FoxtrotChecksum:

    def get_paths(self, uri):
        self._checksum.update(bytes(uri, "utf-8"))
        md5_checksum = self._checksum.get_string()
        self._checksum.reset()
        thumbnail = self._thumbnail_directory.from_md5_checksum(md5_checksum)
        metadata = self._metadata_directory.from_md5_checksum(md5_checksum)
        return md5_checksum, thumbnail, metadata

    def __init__(self):
        self._checksum = GLib.Checksum.new(GLib.ChecksumType.MD5)
        thumbnail_args = "thumbnails", "kusozako_normal_fill", ".png"
        self._thumbnail_directory = FoxtrotCacheDirectory(*thumbnail_args)
        metadata_args = "kusozako-tech", "audio_metadata", ".metadata"
        self._metadata_directory = FoxtrotCacheDirectory(*metadata_args)
