
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from pymediainfo import MediaInfo
from libkusozako3 import ReadableTime
from gi.repository import GLib


def _get_duration(uri):
    path, _ = GLib.filename_from_uri(uri)
    media_info = MediaInfo.parse(path)
    for track in media_info.tracks:
        if track.track_type == "Audio":
            return int(track.duration/1000)
    return 0


def from_uri(uri):
    duration = _get_duration(uri)
    if duration == 0:
        return 0, "?"
    return duration, ReadableTime.from_seconds(duration)
