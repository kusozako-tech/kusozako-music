
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .checksum.Checksum import FoxtrotChecksum
from .key_file.KeyFile import FoxtrotKeyFile


class FoxtrotMetadata:

    @classmethod
    def get_default(cls):
        if "_unique_instance" not in dir(cls):
            cls._unique_instance = cls()
        return cls._unique_instance

    def from_uri(self, uri):
        paths = self._checksum.get_paths(uri)
        key_file = self._key_file.new(uri, *paths)
        return key_file

    def __init__(self):
        self._key_file = FoxtrotKeyFile()
        self._checksum = FoxtrotChecksum()
