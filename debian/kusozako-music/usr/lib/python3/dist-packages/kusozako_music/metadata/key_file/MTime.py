
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio


def get_mtime(uri):
    gio_file = Gio.File.new_for_uri(uri)
    file_info = gio_file.query_info("time::modified", 0)
    return file_info.get_attribute_uint64("time::modified")


def older_than(uri_alfa, uri_bravo):
    mtime_alfa = get_mtime(uri_alfa)
    mtime_bravo = get_mtime(uri_bravo)
    return mtime_alfa > mtime_bravo
