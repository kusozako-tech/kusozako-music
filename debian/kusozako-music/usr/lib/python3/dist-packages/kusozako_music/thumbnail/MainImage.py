
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libkusozako3.Ux import Unit
from libkusozako3.audio_thumbnail.AudioThumbnail import FoxtrotAudioThumbnail
from libkusozako3.thumbnail.Thumbnail import FoxtrotThumbnail
from libkusozako3.thumbnail import Type

SIZE = Unit(16)
ICON_NAME = "audio-x-generic"
ICON_SIZE = Unit(8)
FLAG = Gtk.IconLookupFlags.GENERIC_FALLBACK


class FoxtrotMainImage:

    def _load_from_thumbnail_path(self, thumbnail_path, key_file):
        source_uri = key_file.get_string("metadata", "uri")
        pixbuf = self._audio_thumbnail.get_for_uri(source_uri)
        new_status = "invalid" if pixbuf is None else "valid"
        if new_status == "valid":
            pixbuf.savev(thumbnail_path, "png", [], [])
        key_file.set_string("metadata", "thumbnail_status", new_status)
        key_file_uri = key_file.get_string("metadata", "metadata_path")
        key_file_path, _ = GLib.filename_from_uri(key_file_uri)
        key_file.save_to_file(key_file_path)
        return pixbuf if pixbuf is not None else self._fallback_icon

    """
    def from_key_file(self, key_file):
        thumbnail_status = key_file.get_string("metadata", "thumbnail_status")
        thumbnail_path = key_file.get_string("metadata", "thumbnail_path")
        if thumbnail_status == "valid":
            return GdkPixbuf.Pixbuf.new_from_file(thumbnail_path)
        elif thumbnail_status == "invalid":
            return self._fallback_icon
        return self._load_from_thumbnail_path(thumbnail_path, key_file)
    """

    def get_for_uri(self, gio_file):
        uri = gio_file.get_uri()
        self._checksum.update(bytes(uri, "utf-8"))
        md5_checksum = self._checksum.get_string()
        self._checksum.reset()
        path = gio_file.get_path()
        pixbuf, path = self._thumbnail.load_from_cache(path, md5_checksum)
        if pixbuf is not None:
            return pixbuf
        icon = self._audio_thumbnail.get_for_uri(uri)
        return icon if icon is not None else self._fallback_icon

    def __init__(self):
        self._checksum = GLib.Checksum(GLib.ChecksumType.MD5)
        self._thumbnail = FoxtrotThumbnail(Type.NORMAL_FILL)
        self._audio_thumbnail = FoxtrotAudioThumbnail(SIZE)
        icon_theme = Gtk.IconTheme.get_default()
        self._fallback_icon = icon_theme.load_icon(ICON_NAME, ICON_SIZE, FLAG)
