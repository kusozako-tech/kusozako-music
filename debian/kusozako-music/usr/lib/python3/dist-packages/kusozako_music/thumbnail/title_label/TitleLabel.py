
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Ux import Unit
from .pango_layout.PangoLayout import FoxtrotPangoLayout

TILE_SIZE = Unit(16)
MARGIN = Unit(0.5)
SHADE_COLOR_RGBA = 25/256, 25/256, 112/256, 0.7


class FoxtrotTitleLabel:

    def _get_label_geometries(self, inner_height):
        outer_height = inner_height+MARGIN*2
        return 0, TILE_SIZE-outer_height, TILE_SIZE, outer_height

    def _paint_shade(self, cairo_context, height):
        cairo_context.set_source_rgba(*SHADE_COLOR_RGBA)
        cairo_context.rectangle(*self._get_label_geometries(height))
        cairo_context.fill()

    def paint(self, cairo_context, title):
        layout = FoxtrotPangoLayout(cairo_context, title)
        _, height = layout.get_pixel_size()
        self._paint_shade(cairo_context, height)
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(MARGIN, TILE_SIZE-height-MARGIN)
        layout.paint()
