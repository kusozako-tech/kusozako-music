
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .surface.Surface import FoxtrotSurface
from .MainImage import FoxtrotMainImage
from .title_label.TitleLabel import FoxtrotTitleLabel


class DeltaThumbnail(DeltaEntity):

    """
    def from_key_file(self, key_file):
        surface = FoxtrotSurface.new_tile()
        main_image = self._main_image.from_key_file(key_file)
        cairo_context = surface.paint_pixbuf(main_image)
        title = key_file.get_string("metadata", "title")
        self._title_label.paint(cairo_context, title)
        return main_image, surface.get_pixbuf()
    """

    def get_for_file_and_title(self, gio_file, title):
        surface = FoxtrotSurface.new_tile()
        main_image = self._main_image.get_for_uri(gio_file)
        cairo_context = surface.paint_pixbuf(main_image)
        self._title_label.paint(cairo_context, title)
        return main_image, surface.get_pixbuf()

    def __init__(self, parent):
        self._parent = parent
        self._main_image = FoxtrotMainImage()
        self._title_label = FoxtrotTitleLabel()
